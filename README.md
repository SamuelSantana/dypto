# dypto

É um app desenvolvido em fluter, com o intuito mostrar as melhores cotações de criptomoedas por exchanges localizadas na argentina, é um app baseado em requisições http de api's de cotação. Ele também conta com um servidor Nodejs.

A dypto foi desenvolvida com o propósito de ajudar ao negociador a ter um melhor acompanhamento dos melhores rulos existentes no mercado, os preços são atualizados automaticamente a cada 1 minuto. O preço da Criptomoeda é de total responsabilidade de cada provedor, com isso, a Dypto não se responsabiliza pela mudança de preço, nos preços já estão inclusos as taxas e impostos. 

O principal papel da Dypto é notificar ,a partir dos filtros selecionados, quando os preços chegarem em determinado valor, ajudando ao negociador a fazer os melhores rulos.
