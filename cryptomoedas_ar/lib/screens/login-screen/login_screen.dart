import 'package:cryptomoedas_ar/components/helpers/validator.dart';
import 'package:cryptomoedas_ar/models/user.dart';
import 'package:cryptomoedas_ar/models/user_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:provider/provider.dart';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoginScreen extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();

  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      backgroundColor: kPrimaryColor,
      key: scaffoldKey,
      // appBar: AppBar(
      //   title: const Text("criptomonedas"),
      //   centerTitle: true,
      //   actions: [
      //     FlatButton(
      //       onPressed: () {
      //         Navigator.of(context).pushReplacementNamed('/signup');
      //       },
      //       textColor: Colors.white,
      //       child: Text(
      //         'criar conta',
      //         style: TextStyle(
      //           fontSize: 14,
      //         ),
      //       ),
      //     ),
      //   ],
      // ),
      body: Center(
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 70, top: 30),
              padding: EdgeInsets.all(5),
              height: 110,
              child: SvgPicture.asset(
                "assets/images/logo_dypto.svg",
              ),
            ),
            Container(
              child: Form(
                key: formkey,
                child: Consumer<UserManager>(
                  builder: (_, UserManager, __) {
                    return Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          Container(
                            padding:
                                EdgeInsets.only(left: 10, right: 10, bottom: 0),
                            child: TextFormField(
                              controller: emailController,
                              enabled: !UserManager.loading,
                              decoration: InputDecoration(hintText: 'E-mail'),
                              keyboardType: TextInputType.emailAddress,
                              autocorrect: false,
                              validator: (email) {
                                if (!emailValid(email)) {
                                  return "Email inválido";
                                }
                                return null;
                              },
                            ),
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(15),
                                bottomLeft: Radius.circular(15),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: 10, right: 10, bottom: 1, top: 0),
                            child: TextFormField(
                              controller: passController,
                              enabled: !UserManager.loading,
                              decoration: InputDecoration(hintText: 'Senha'),
                              obscureText: true,
                              autocorrect: false,
                              validator: (pass) {
                                if (pass.isEmpty || pass.length < 4)
                                  return 'Senha inválida';
                                return null;
                              },
                            ),
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(15),
                                bottomLeft: Radius.circular(15),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "¿Olvidaste tu Contraseña? ",
                                  style: TextStyle(
                                    color: kSecondaryColor,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.of(context)
                                        .pushNamed('/forgotPassword');
                                    //.pushNamed('/latepayment');
                                  },
                                  //padding: EdgeInsets.zero,
                                  child: Text(
                                    "Clic aquí",
                                    style: TextStyle(
                                      color: kSecondaryColor,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          GestureDetector(
                            onTap: UserManager.loading
                                ? null
                                : () {
                                    if (formkey.currentState.validate()) {
                                      UserManager.signIn(
                                        user: User(
                                          email: emailController.text,
                                          password: passController.text,
                                        ),
                                        onFail: (e) {
                                          scaffoldKey.currentState.showSnackBar(
                                            SnackBar(
                                              content:
                                                  Text("Error al ingresar: $e"),
                                              backgroundColor: Colors.red,
                                            ),
                                          );
                                        },
                                        onSuccess: () {
                                          Navigator.of(context)
                                              .pushReplacementNamed('/home');
                                        },
                                      );
                                    }
                                  },
                            //color: kInfoColor,
                            // disabledColor: kSecondaryColor,
                            //textColor: Colors.white,
                            child: Container(
                              padding: EdgeInsets.all(15),
                              child: UserManager.loading
                                  ? Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.all(2),
                                              child: SizedBox(
                                                height: 15,
                                                width: 15,
                                                child:
                                                    CircularProgressIndicator(
                                                  valueColor:
                                                      AlwaysStoppedAnimation(
                                                          Colors.white),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              " Entrando",
                                              style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    )
                                  : Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "Entrar",
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: kInfoColor,
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(15),
                                ),
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 35, bottom: 10),
                                child: Row(
                                  children: [
                                    Text(
                                      "¿No tenes una cuenta?",
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: GestureDetector(
                                        onTap: () {
                                          // Navigator.pop(context);
                                          Navigator.of(context)
                                              .pushNamed('/signup');
                                        },
                                        //padding: EdgeInsets.zero,
                                        child: Text(
                                          " Registrate",
                                          style: TextStyle(
                                            color: kSecondaryColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 16,
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
