import 'package:cryptomoedas_ar/API/provedor.dart';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/screens/sell_screen/components/list_bid_ars.dart';
import 'package:flutter/material.dart';

class BodyTablePages extends StatelessWidget {
  const BodyTablePages({
    Key key,
    @required this.titlePage,
    @required this.nameColum1,
    @required this.nameColum2,
    // @required this.provedor,
    // @required this.loading,
  }) : super(key: key);

  final String titlePage;
  final String nameColum1;
  final String nameColum2;
  //final List<Provedor> provedor;
  //final bool loading;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 16, right: 16),
      child: Container(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Text(
                titlePage,
                style: TextStyle(fontSize: 30, color: Colors.white),
              ),
            ),
            Expanded(
              child: Container(
                //margin: EdgeInsets.only(bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      child: Container(
                        //padding: EdgeInsets.only(bottom: 0),
                        margin: EdgeInsets.only(
                            left: 10, bottom: 20, right: 0, top: 20),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 16, right: 16),
                              padding: EdgeInsets.only(bottom: 8),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                //crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Text(
                                    nameColum1,
                                    style: TextStyle(
                                      fontSize: 17,
                                      color: Colors.black,
                                      //fontWeight: FontWeight.bold,
                                      fontFamily: 'PoiretOne',
                                    ),
                                  ),
                                  Container(
                                    // margin:
                                    //     EdgeInsets.symmetric(horizontal: 10),
                                    child: Text(
                                      nameColum2,
                                      style: TextStyle(
                                        fontSize: 17,
                                        color: Colors.black,
                                        //fontWeight: FontWeight.bold,
                                        fontFamily: 'PoiretOne',
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    width: 1,
                                    color: kSecondaryColor,
                                  ),
                                ),
                              ),
                            ),
                            ListBidArs(),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15),
                    bottomLeft: Radius.circular(15),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 20, left: 10),
              child: Text(
                'Los precios yá son con tasas e impuestos.',
                style: TextStyle(
                  fontSize: 10,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
