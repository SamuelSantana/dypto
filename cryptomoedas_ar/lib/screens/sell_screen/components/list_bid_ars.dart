import 'dart:async';
import 'dart:convert';
import 'package:cryptomoedas_ar/API/api.dart';
import 'package:cryptomoedas_ar/API/provedor.dart';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/models/user_manager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListBidArs extends StatefulWidget {
  @override
  _ListBidArsState createState() => _ListBidArsState();
}

class _ListBidArsState extends State<ListBidArs> {
  var provedor = List<Provedor>();
  Timer timer;
  var loading = false;
  _getDaiBid() {
    API.getDaiBid().then((response) {
      setState(() {
        Iterable lista = json.decode(response.body);
        loading = true;
        provedor = lista.map((model) => Provedor.fromJson(model)).toList();
        loading = false;
      });
    });
  }

  _ListBidArsState() {
    super.initState();
    _getDaiBid();
    timer = Timer.periodic(Duration(seconds: 55), (Timer t) => _getDaiBid());
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var tim = DateTime.now();
    return FutureBuilder(
      future: API.getDaiBid(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Container(
            alignment: Alignment.centerLeft,
            height: MediaQuery.of(context).size.height * 0.6,
            // padding: EdgeInsets.only(
            //     left: 10, right: 10, bottom: 1, top: 0),
            child: Consumer<UserManager>(builder: (_, userManager, __) {
              return ListView.builder(
                itemCount: provedor.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          provedor[index].fields.name,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                        ),
                        Text(
                          provedor[index].fields.ars.formatted_bid,
                          style: TextStyle(fontSize: 20, color: Colors.black),
                        ),
                      ],
                    ),
                    // subtitle: Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //   children: [
                    //     Text(
                    //       'Qnt DAI con 200US\$:' + tim.toString(),
                    //       style:
                    //           TextStyle(fontSize: 10, color: kSecondaryColor),
                    //     ),
                    //     Text(
                    //       (200).toInt().toString(),
                    //       style:
                    //           TextStyle(fontSize: 10, color: kSecondaryColor),
                    //     ),
                    //   ],
                    // ),
                  );
                },
              );
            }),

            //   ],
            // );
          );
        } else if (snapshot.hasError) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 50),
            child: Center(
              child: Card(
                color: kTreeColor,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Text(
                        "No fue posible cargar los datos.",
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        "Verificá tu conexión !",
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        }
        return Center(
          child: Padding(
            padding: const EdgeInsets.all(40),
            child: SizedBox(
              height: 40,
              width: 40,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(kPrimaryColor),
              ),
            ),
          ),
        );
      },
    );
  }
}
