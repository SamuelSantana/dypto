import 'dart:async';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/screens/components/app_bar_pages.dart';
import 'package:cryptomoedas_ar/screens/compras_screen/components/body.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// class ComprasScreen extends StatefulWidget {
//   @override
//   _ComprasScreenState createState() => _ComprasScreenState();
// }

class ComprasScreen extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Container(

//     );
//   }
// }
//class _ComprasScreenState extends State<ComprasScreen> {
  //var provedor = List<Provedor>();
  String titlePage = 'Compra';
  String nameColum1 = 'Exchange';
  String nameColum2 = 'Precio(US\$)';

  Timer timer;
  //var loading = false;

  // _getDaiUsdAsk() {
  //   API.getDaiUsdAsk().then((response) {
  //     setState(() {
  //       Iterable lista = json.decode(response.body);
  //       loading = true;
  //       provedor = lista.map((model) => Provedor.fromJson(model)).toList();
  //       loading = false;
  //     });
  //   });
  // }

  // _ComprasScreenState() {
  //   super.initState();
  //   // _getDaiUsdAsk();
  //   // timer = Timer.periodic(Duration(seconds: 55), (Timer t) => _getDaiUsdAsk());
  // }

  // @override
  // void dispose() {
  //   timer?.cancel();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      backgroundColor: kPrimaryColor,
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0,
        centerTitle: false,
        title: AppBarPages(),
      ),
      body: BodyTablePages(
        titlePage: titlePage,
        nameColum1: nameColum1,
        nameColum2: nameColum2,
        //provedor: provedor,
        //loading: loading),
      ),
    );
  }
}
