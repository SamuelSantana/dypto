import 'dart:async';
import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cryptomoedas_ar/API/api.dart';
import 'package:cryptomoedas_ar/API/api_plus_cambio.dart';
import 'package:cryptomoedas_ar/API/provedor.dart';
import 'package:cryptomoedas_ar/API/quotation.dart';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/models/rulos.dart';
import 'package:cryptomoedas_ar/models/user.dart';
import 'package:cryptomoedas_ar/models/user_manager.dart';
import 'package:cryptomoedas_ar/screens/home_screen/components/app_Bottom_list.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var provedor = List<Provedor>();
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  Timer timer;
  UserManager userManager;
  final Firestore firestore = Firestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;
  var provedorUSD = List<Provedor>();
  var provedorARS = List<Provedor>();
  var quotation = List<QuotationDolar>();
  double dolar = 0;
  DocumentSnapshot user;

  var listRulos = []; //List<Rulo>();
  var rulo = Rulo();
  var _loading = false;

  // _getDaiBid() {
  //   API.getDaiBid().then((response) {
  //     setState(() {
  //       Iterable lista = json.decode(response.body);
  //       provedor = lista.map((model) => Provedor.fromJson(model)).toList();
  //     });
  //   });
  // }
  bool get loading => _loading;
  set loading(bool value) {
    _loading = value;
  }

  _getDaiRulos() async {
    loading = false;
    final FirebaseUser currentUser = await auth.currentUser();

    final DocumentSnapshot user =
        await firestore.collection('users').document(currentUser.uid).get();
    API_Dolar.getDollarOficial().then((response) {
      Iterable lista = json.decode(response.body);
      quotation = lista.map((model) => QuotationDolar.fromJson(model)).toList();
      dolar = double.parse(quotation[0].sell) * 1.3;
    });

    API.getDaiUsdAsk().then((response) {
      Iterable lista = json.decode(response.body);
      provedorUSD = lista.map((model) => Provedor.fromJson(model)).toList();

      API.getDaiBid().then((response) {
        setState(() {
          listRulos = [];
          Iterable lista = json.decode(response.body);
          //loading = true;
          provedorARS = lista.map((model) => Provedor.fromJson(model)).toList();

          // passar as variáveis pelos filtros de cada cliente
          var retira_exchange = false;
          provedorUSD.forEach((usd) {
            provedorARS.forEach((ars) {
              if (!user.data['providers_satoshiTango'] &&
                  (ars.fields.name == 'Satoshitango' ||
                      usd.fields.name == 'Satoshitango')) {
                retira_exchange = true;
              }
              if (!user.data['providers_seSocio'] &&
                  (ars.fields.name == 'SeSocio' ||
                      usd.fields.name == 'SeSocio')) {
                retira_exchange = true;
              }
              if (!user.data['providers_qubit'] &&
                  (ars.fields.name == 'Qubit Brokers' ||
                      usd.fields.name == 'Qubit Brokers')) {
                retira_exchange = true;
              }
              if (!user.data['providers_decrypto'] &&
                  (ars.fields.name == 'Decrypto' ||
                      usd.fields.name == 'Decrypto')) {
                retira_exchange = true;
              }
              if (!user.data['providers_buenbit'] &&
                  (ars.fields.name == 'Buenbit 2.0' ||
                      usd.fields.name == 'Buenbit 2.0')) {
                retira_exchange = true;
              }
              if (!user.data['providers_ripio'] &&
                  (ars.fields.name == 'Ripio' || usd.fields.name == 'Ripio')) {
                retira_exchange = true;
              }
              if (!user.data['providers_ripioExchange'] &&
                  (ars.fields.name == 'Ripio Exchange' ||
                      usd.fields.name == 'Ripio Exchange')) {
                retira_exchange = true;
              }

              if (retira_exchange == false) {
                var totalInvest = (200 * dolar);
                var totalSell =
                    (((200 / usd.fields.usd.ask).toInt()) * ars.fields.ars.bid);
                var gain = totalSell - totalInvest;
                var percent = (gain * 100) / totalInvest;

                rulo = Rulo(
                    name_ask: usd.fields.name,
                    ask: usd.fields.usd.ask,
                    bid: ars.fields.ars.bid,
                    name_bid: ars.fields.name,
                    percent: percent,
                    gain: gain);
                listRulos.add(rulo);
              }

              retira_exchange = false;
            });
          });

          rulo = Rulo(
              name_ask: 'No hay rulos',
              ask: 0.0,
              bid: 0.0,
              name_bid: 'No hay rulos',
              percent: 0.0,
              gain: 0.0);
          listRulos.add(rulo);

          listRulos.sort((a, b) => b.gain.compareTo(a.gain));
          loading = true;
        });
      });
    });
  }

  void expiredPayment() async {
    DateTime dateNow = DateTime.now();
    final FirebaseUser currentUser = await auth.currentUser();

    if (currentUser == null) {
      Navigator.of(context).pushReplacementNamed('/login');
    } else {
      user =
          await firestore.collection('users').document(currentUser.uid).get();
      final DateTime paymentDate = user.data['payment_date'].toDate();

      // Navigator.of(context).pushReplacementNamed('/home');

      if (dateNow.difference(paymentDate).inDays > 0) {
        // tem que colocar para a tela explicativa de vencimento do prazo.
        Navigator.of(context).pushReplacementNamed('/latepayment');
      }
    }
  }

  _HomeScreenState() {
    super.initState();
    expiredPayment();
    _getDaiRulos();
    this.startFirebaseListeners();

    timer = Timer.periodic(Duration(seconds: 50), (Timer t) => _getDaiRulos());
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  void startFirebaseListeners() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('Mensagem: $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('Resume: $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('Launch: $message');
      },
    );

    _firebaseMessaging.getToken().then((token) {
      print("Token: $token");
    });

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registred: $settings");
    });
  }

  Widget build(BuildContext context) {
    //var nowe = new DateTime.now();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: FutureBuilder(
          future: API_Dolar.getDollarOficial(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Padding(
                padding: const EdgeInsets.only(top: 40, left: 16, right: 16),
                child: Container(
                  height: double.infinity,
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  SizedBox(
                                    height: 70,
                                    child: Container(
                                      //color: kGreenColor,
                                      width: 70,
                                      margin: EdgeInsets.all(16),
                                      alignment: Alignment.topCenter,
                                      child: SvgPicture.asset(
                                        "assets/images/logo_dypto.svg",
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      //color: kGreenColor,
                                      margin: EdgeInsets.only(right: 20),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        // mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Cotacion Dolar Oficial(+impuestos): ",
                                            style: TextStyle(
                                                fontSize: 11,
                                                color: kSecondaryColor),
                                          ),
                                          Text(
                                            dolar != 0
                                                ? "\$ " +
                                                    dolar.toStringAsPrecision(4)
                                                : 'Cargando',
                                            style: TextStyle(
                                                fontSize: 13,
                                                color: kSecondaryColor),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 20),
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: SizedBox(
                                    height: 60,
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.of(context)
                                            .pushNamed('/filter');
                                      },
                                      child: Container(
                                        height: 20,
                                        width: 25,
                                        margin: EdgeInsets.only(right: 0),
                                        child: SvgPicture.asset(
                                          'assets/icons/filter-cinza.svg',
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 50),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Criptomoneda:',
                                      style: TextStyle(
                                          fontSize: 12, color: kSecondaryColor),
                                    ),
                                    Row(
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 15),
                                          child: Container(
                                            height: 40,
                                            width: 40,
                                            margin: EdgeInsets.only(right: 0),
                                            child: SvgPicture.asset(
                                              'assets/icons/dai.svg',
                                            ),
                                          ),
                                        ),
                                        Text(
                                          'DAI',
                                          style: TextStyle(
                                            fontFamily: 'PoiretOne',
                                            fontSize: 40,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 40,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Ganancia',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: kSecondaryColor),
                                            ),
                                            Text(
                                              loading
                                                  ? '\$ ' +
                                                      listRulos[0]
                                                          .gain
                                                          .toStringAsPrecision(
                                                              6)
                                                  : "Carg...",
                                              style: TextStyle(
                                                fontSize: 30,
                                              ),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.only(right: 10),
                                          //color: kBlackColor,
                                          child: Text(
                                            loading
                                                ? "% " +
                                                    listRulos[0]
                                                        .percent
                                                        .toStringAsPrecision(4)
                                                : "Carg...",
                                            style: TextStyle(
                                              fontSize: 20,
                                              color: kGreenColor,
                                            ),
                                          ),
                                          decoration: BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            border:
                                                Border.all(color: kGreenColor),
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(15),
                                              bottomLeft: Radius.circular(30),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 60,
                              ),
                              Flexible(
                                fit: FlexFit.tight,
                                child: Container(
                                  //color: kGreenColor,
                                  padding: EdgeInsets.only(bottom: 0),
                                  margin: EdgeInsets.only(
                                      left: 20, bottom: 20, right: 20),
                                  child: Row(
                                    //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      Flexible(
                                        fit: FlexFit.tight,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Comprás en:',
                                              style: TextStyle(
                                                fontSize: 12,
                                                color: kSecondaryColor,
                                              ),
                                            ),
                                            Text(
                                              loading
                                                  ? listRulos[0]
                                                      .name_ask
                                                      .toString()
                                                  : "Cargando...",
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: kSecondaryColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 40,
                                      ),
                                      Flexible(
                                        fit: FlexFit.tight,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Vendés en',
                                              style: TextStyle(
                                                fontSize: 12,
                                                color: kSecondaryColor,
                                              ),
                                            ),
                                            Text(
                                              loading
                                                  ? listRulos[0]
                                                      .name_bid
                                                      .toString()
                                                  : "Cargando...",
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: kSecondaryColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              // Container(
                              //   height: 300,
                              //   padding: EdgeInsets.only(
                              //       left: 10, right: 10, bottom: 1, top: 0),
                              //   child: Consumer<UserManager>(
                              //     builder: (_, userManager, __) {
                              //       return
                              //           // Column(
                              //           //   children: [
                              //           // GestureDetector(
                              //           //   onTap: () {
                              //           //     if (userManager.isLoggedin) {
                              //           //       userManager.signOut();
                              //           //       Navigator.of(context).pushReplacementNamed('/login');
                              //           //     } else {
                              //           //       Navigator.of(context).pushReplacementNamed('/login');
                              //           //     }
                              //           //   },
                              //           //   child: Text(
                              //           //     userManager.isLoggedin ? 'Sair' : 'Entrar',
                              //           //     style: TextStyle(
                              //           //       fontSize: 14,
                              //           //     ),
                              //           //   ),
                              //           // ),
                              //           ListView.builder(
                              //         itemCount: provedor.length,
                              //         itemBuilder: (context, index) {
                              //           return ListTile(
                              //             title: Text(
                              //               provedor[index].fields.name,
                              //               style: TextStyle(
                              //                   fontSize: 10, color: Colors.black),
                              //             ),
                              //             subtitle: Text(
                              //               nowe.toString(),
                              //               style: TextStyle(
                              //                   fontSize: 10, color: Colors.black),
                              //             ),
                              //           );
                              //         },
                              //       );
                              //       //   ],
                              //       // );
                              //     },
                              //   ),
                              // ),
                            ],
                          ),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(15),
                              bottomLeft: Radius.circular(15),
                            ),
                          ),
                        ),
                      ),
                      AppBottomList()
                    ],
                  ),
                ),
              );
            } else if (snapshot.hasError) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 50),
                child: Center(
                  child: Card(
                    color: kTreeColor,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Text(
                            "No fue posible cargar los datos.",
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            "Verificá tu conexión !",
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }
            return Center(
              child: Padding(
                padding: const EdgeInsets.all(40),
                child: SizedBox(
                  height: 40,
                  width: 40,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(kPrimaryColor),
                  ),
                ),
              ),
            );
          }),
    );
  }
}
