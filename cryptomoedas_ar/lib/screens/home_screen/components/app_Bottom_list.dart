import 'package:cryptomoedas_ar/screens/home_screen/components/app_Botom_item.dart';
import 'package:flutter/material.dart';

class AppBottomList extends StatelessWidget {
  const AppBottomList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          AppBottomItem(
            icon: 'assets/icons/sell.png',
            title: 'Ventas',
            press: () {
              Navigator.of(context).pushNamed('/sell');
            },
          ),
          AppBottomItem(
            icon: 'assets/icons/pay.png',
            title: 'Compras',
            press: () {
              Navigator.of(context).pushNamed('/compras');
            },
          ),
          AppBottomItem(
            icon: 'assets/icons/rulos.png',
            title: 'Rulos',
            press: () {
              Navigator.of(context).pushNamed('/rulos');
            },
          ),
          AppBottomItem(
            icon: 'assets/icons/insert_card.png',
            title: 'Pago',
            press: () {
              Navigator.of(context).pushNamed('/payment');
            },
          ),
          AppBottomItem(
            icon: 'assets/icons/perfil.png',
            title: 'Perfil',
            press: () {
              Navigator.of(context).pushNamed('/perfil');
            },
          ),
        ],
      ),
    );
  }
}
