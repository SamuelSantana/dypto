import 'package:flutter/material.dart';

class AppBottomItem extends StatelessWidget {
  final String icon;
  final String title;
  final Function press;
  const AppBottomItem({
    Key key,
    this.icon,
    this.title,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        height: 90,
        width: 96,
        margin: EdgeInsets.only(right: 20, top: 20, bottom: 20),
        padding: EdgeInsets.all(5),
        child: Column(
          children: [
            Text(title),
            SizedBox(
              height: 10,
            ),
            Image.asset(
              icon,
              height: 40,
              // width: 20,
            ),
          ],
        ),
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(15),
            bottomLeft: Radius.circular(15),
          ),
        ),
      ),
    );
  }
}
