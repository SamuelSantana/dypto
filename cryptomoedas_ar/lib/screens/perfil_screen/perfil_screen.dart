import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/models/user_manager.dart';
import 'package:cryptomoedas_ar/screens/components/app_bar_icon_simple.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class PerfilScreen extends StatefulWidget {
  @override
  _PerfilScreenState createState() => _PerfilScreenState();
}

class _PerfilScreenState extends State<PerfilScreen> {
  final Firestore firestore = Firestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;

  var name;
  var dni;
  var email;
  var entry;

  @override
  void initState() {
    getPerfil();
    super.initState();
  }

  getPerfil() async {
    final FirebaseUser currentUser = await auth.currentUser();

    var user =
        await firestore.collection('users').document(currentUser.uid).get();

    entry = user.data['start_registration'].toDate();
    email = user.data['email'];
    dni = user.data['dni'];
    name = user.data['name'];
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      backgroundColor: kPrimaryColor,
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0,
        centerTitle: true,
        title: AppBarIconSimple(),
      ),
      body: Consumer<UserManager>(
        builder: (_, UserManager, __) {
          return FutureBuilder(
            future: getPerfil(),
            builder: (_, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: SizedBox(
                    height: 50,
                    width: 50,
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(Colors.white),
                    ),
                  ),
                );
              } else {
                return Container(
                  height: double.infinity,
                  width: double.infinity,
                  child: ListView(
                    children: [
                      Container(
                        margin:
                            EdgeInsets.only(left: 16, right: 16, bottom: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: 30),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Text(
                                      'Perfil',
                                      style: TextStyle(
                                          fontSize: 30, color: Colors.white),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      UserManager.signOut();
                                      Navigator.pop(context);
                                      Navigator.of(context)
                                          .pushReplacementNamed('/login');
                                    },
                                    child: Row(
                                      children: [
                                        Container(
                                          height: 15,
                                          width: 25,
                                          margin: EdgeInsets.only(right: 0),
                                          child: Image.asset(
                                            'assets/icons/logout.png',
                                          ),
                                        ),
                                        Text(
                                          'Salir',
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(bottom: 20),
                                    padding: EdgeInsets.only(bottom: 8),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Informaciones Personales',
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.white,
                                            //fontWeight: FontWeight.bold,
                                            fontFamily: 'PoiretOne',
                                          ),
                                        ),
                                      ],
                                    ),
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          width: 1,
                                          color: kSecondaryColor,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      'Nombre:',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Container(
                                      child: Text(
                                        name.toString(),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      'DNI:',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Container(
                                      child: Text(
                                        dni.toString(),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      'Email:',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Container(
                                      child: Text(
                                        email.toString(),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      'Ingresaste en:',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10),
                                    child: Container(
                                      child: Text(
                                        DateFormat('dd/MM/y').format(entry),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                  // Container(
                                  //   child: Text(
                                  //     'Cambiar la Contraseña:',
                                  //     style: TextStyle(
                                  //       color: Colors.white,
                                  //     ),
                                  //   ),
                                  // ),
                                  // Padding(
                                  //   padding: const EdgeInsets.symmetric(
                                  //       vertical: 10),
                                  //   child: Container(
                                  //     padding: EdgeInsets.all(8),
                                  //     child: Text(
                                  //       'Cambiar Contraseña',
                                  //       style: TextStyle(
                                  //         color: Colors.white,
                                  //         fontSize: 16,
                                  //       ),
                                  //     ),
                                  //     decoration: BoxDecoration(
                                  //       shape: BoxShape.rectangle,
                                  //       color: kTreeColor,
                                  //       borderRadius: BorderRadius.only(
                                  //         topRight: Radius.circular(15),
                                  //         bottomLeft: Radius.circular(15),
                                  //       ),
                                  //     ),
                                  //   ),
                                  // ),
                                  Container(
                                    margin:
                                        EdgeInsets.only(bottom: 10, top: 25),
                                    padding: EdgeInsets.only(bottom: 8),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Sobre la aplicacíon',
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.white,
                                            //fontWeight: FontWeight.bold,
                                            fontFamily: 'PoiretOne',
                                          ),
                                        ),
                                      ],
                                    ),
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          width: 1,
                                          color: kSecondaryColor,
                                        ),
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 10),
                                      child: Container(
                                        padding: EdgeInsets.all(8),
                                        child: Text(
                                          'Leer mas',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                          ),
                                        ),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          color: kInfoColor,
                                          borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(15),
                                            bottomLeft: Radius.circular(15),
                                          ),
                                        ),
                                      ),
                                    ),
                                    onTap: () {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) =>
                                            StatefulBuilder(
                                          builder: (context, setState) {
                                            return SimpleDialog(
                                                title: Text('Dypto'),
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            16),
                                                    child: Text(
                                                      'Somos la dypto y estamos aquí para ayudarlo a aprovechar las mejores oportunidades de rulos, enviándole notificaciones cuando llegue en los precios establecidos por usted. ',
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            16),
                                                    child: Text(
                                                      'En nuestra aplicación encontrará los precios que ofrece cada proveedor de compra(Dolar) y venta(Pesos). Así como las posibles ganancias si el rulo se realiza con los proveedores mostrados.',
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            16),
                                                    child: Text(
                                                      'Es posible ver el precio del dólar oficial, en tiempo real y a su mejor precio, (https://pluscambio.com.ar/) y también los precios del DAI ya con impuestos y tasas, por lo tanto en su precio final y en tiempo real (Actualización automática cada 1 min).',
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            16),
                                                    child: Text(
                                                      'Nuestra función principal es ayudarte a no perder la mejor oportunidad para poder sacar más provecho de los rulos sin tener que estar pendiente todo el tiempo.',
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            16),
                                                    child: Text(
                                                      'Para eso necesitamos su ayuda para mantener el sistema en el aire y ofrecer siempre el mejor servicio. En la pestaña de pagos tendrás el monto, que puede variar según Blue.',
                                                    ),
                                                  ),
                                                ]);
                                          },
                                        ),
                                      ).then((returnVal) {
                                        if (returnVal != null) {
                                          Scaffold.of(context).showSnackBar(
                                            SnackBar(
                                              content:
                                                  Text("Clicou: $returnVal"),
                                              action: SnackBarAction(
                                                label: 'Ok',
                                                onPressed: () {},
                                              ),
                                            ),
                                          );
                                        }
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }
            },
          );
        },
      ),
    );
  }
}
