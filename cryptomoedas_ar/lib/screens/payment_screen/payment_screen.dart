import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/models/payment.dart';
import 'package:cryptomoedas_ar/models/user_manager.dart';
import 'package:cryptomoedas_ar/screens/components/app_bar_icon_simple.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mercado_pago_mobile_checkout/mercado_pago_mobile_checkout.dart';

import 'package:mercadopago_sdk/mercadopago_sdk.dart';
import 'package:provider/provider.dart';

import 'package:cryptomoedas_ar/utils/globals.dart' as globals;

class PaymentScreen extends StatefulWidget {
  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  String _platformVersion = 'Unknown';
  bool _loading = false;
  bool get loading => _loading;
  var last_payment;
  var date_payment;
  var monthly_value;
  var start_registration;

  //Payment _payment;

  final Firestore firestore = Firestore.instance;

  final FirebaseAuth auth = FirebaseAuth.instance;

  @override
  void initState() {
    getPayment();
    super.initState();

    initPlatformState();

    //mercadoPago();
  }

  set loading(bool value) {
    _loading = value;
    //notifyListeners();
  }

  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await MercadoPagoMobileCheckout.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  getPayment() async {
    final FirebaseUser currentUser = await auth.currentUser();

    var detail_payment_user =
        await firestore.collection('users').document(currentUser.uid).get();
    var value_payment = await firestore
        .collection('monthly_payment')
        .document('SUFDN3eWrTsfpFaDYiQp')
        .get();

    monthly_value = value_payment.data['unit_price'].toStringAsPrecision(5);

    date_payment = detail_payment_user.data['payment_date'].toDate();
    last_payment = detail_payment_user.data['last_payment'].toDate();
    start_registration =
        detail_payment_user.data['start_registration'].toDate();
    if (start_registration.difference(last_payment).inDays == 0 &&
        date_payment.difference(start_registration).inDays == 2) {
      last_payment = '- / - / -';
    } else {
      last_payment = DateFormat('dd/MM/y').format(last_payment);
    }
  }

  Future<Map<String, dynamic>> armarPreferencia() async {
    final FirebaseUser currentUser = await auth.currentUser();
    final DocumentSnapshot user =
        await firestore.collection('users').document(currentUser.uid).get();

    final DocumentSnapshot config_payment = await firestore
        .collection('monthly_payment')
        .document('SUFDN3eWrTsfpFaDYiQp')
        .get();
    // configurar toda a preferencia
    var mp = MP(globals.mpClientID, globals.mpClienteSecret);
    var preference = {
      "items": [
        {
          "title": config_payment.data['title'],
          "description": config_payment.data['description'],
          "quantity": config_payment.data['quantity'],
          "currency_id": config_payment.data['currency_id'],
          "unit_price": config_payment.data['unit_price'],
        }
      ],
      "payer": {
        "name": user.data['name'],
        "email": user.data['email'],
        "date_created": DateTime.now().toString(),
        "DNI": user.data['dni'],
      },
      "payment_methods": {
        "excluded_payment_types": [
          {"id": "ticket"},
          {"id": "atm"}
        ],
        "default_installments": 1,
      }
    };

    var result = await mp.createPreference(preference);
    return result;
  }

  telaMercadoPago(preference_id) async {
    var datePayment;
    var mercadoPagoMobileCheckout =
        await MercadoPagoMobileCheckout.startCheckout(
      globals.mpTestPublicKey,
      preference_id,
    );
    print('teste ----------------------------');
    if (mercadoPagoMobileCheckout.status == 'approved') {
      print(mercadoPagoMobileCheckout);
      final FirebaseUser currentUser = await auth.currentUser();

      // FAZENDO A ALTERAÇÃO DA DATA DE PAGAMENTO NO BANCO
      var detail_payment_user =
          await firestore.collection('users').document(currentUser.uid).get();
      print(
          'datePayment -----------------------------------------------------------');

      datePayment = detail_payment_user.data['payment_date'];
      datePayment = datePayment.toDate();
      // Verificar se a data do pagamento foi pago depois do vencimento acrescentará 30 dias da data de pagamento
      if (datePayment.difference(DateTime.now()).inDays < 0) {
        datePayment = DateTime.now().add(Duration(days: 30));
      } else {
        datePayment = datePayment.add(Duration(days: 30));
      }

      firestore.collection('users').document(currentUser.uid).updateData(
          {'payment_date': datePayment, 'last_payment': DateTime.now()});

      var user_payments = await firestore
          .collection('users')
          .document(currentUser.uid)
          .collection('payments')
          .add({
        'result': mercadoPagoMobileCheckout.result,
        'id': mercadoPagoMobileCheckout.id,
        'status': mercadoPagoMobileCheckout.status,
        'statusDetail': mercadoPagoMobileCheckout.statusDetail,
        'paymentMethodId': mercadoPagoMobileCheckout.paymentMethodId,
        'paymentTypeId': mercadoPagoMobileCheckout.paymentTypeId,
        'transactionAmount': mercadoPagoMobileCheckout.transactionAmount,
        'issuerId': mercadoPagoMobileCheckout.issuerId,
        'paymentDate:': DateTime.now(),
        'errorMessage': mercadoPagoMobileCheckout.errorMessage
      });
      Navigator.of(context).pop();
      Navigator.of(context).pushReplacementNamed('/home');
      Navigator.of(context).pushNamed('/finalpayment');
    } else {
      print('Não aproved');
    }
  }

  Future<void> mercadoPago() async {
    loading = true;

    var preference_id;
    armarPreferencia().then((result) async {
      if (result != null) {
        var preferenceID = result['response']['id'];

        preference_id = preferenceID.toString();
        telaMercadoPago(preference_id);
      }
    });
    loading = false;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      backgroundColor: kPrimaryColor,
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0,
        centerTitle: true,
        title: AppBarIconSimple(),
      ),
      body: Consumer<UserManager>(builder: (_, userManager, __) {
        return FutureBuilder(
          future: getPayment(),
          builder: (_, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: SizedBox(
                  height: 40,
                  width: 40,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.white),
                  ),
                ),
              );
            } else {
              return Container(
                height: double.infinity,
                //width: double.infinity,
                child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              'Forma de Pago',
                              style:
                                  TextStyle(fontSize: 30, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      Flexible(
                        fit: FlexFit.tight,
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(bottom: 30),
                                padding: EdgeInsets.only(bottom: 8),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Informaciones',
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                        //fontWeight: FontWeight.bold,
                                        fontFamily: 'PoiretOne',
                                      ),
                                    ),
                                    GestureDetector(
                                      child: Container(
                                        child: Image.asset(
                                          'assets/icons/info.png',
                                          height: 20,
                                          // width: 20,
                                        ),
                                      ),
                                      onTap: () {
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              StatefulBuilder(
                                            builder: (context, setState) {
                                              return SimpleDialog(
                                                  title: Text('Pagamento'),
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              16),
                                                      child: Text(
                                                        'Dypto te da 3 días para probar nuestros servicios. ',
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              16),
                                                      child: Text(
                                                        'Durante los primeros 15 días del lanzamiento de la aplicación, solo se cobrará 1 dólar blue por mes. A partir de esta fecha, se puede reajustar.',
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              16),
                                                      child: Text(
                                                        'El pago lo realizará la propia aplicación, la cual abrirá el sistema de mercado de pago haciendo clic en pagar, con lo cual se te liberará 30 días para que puedas disfrutar de nuestros servicios.',
                                                      ),
                                                    ),
                                                  ]);
                                            },
                                          ),
                                        );
                                      },
                                    ),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                  border: Border(
                                    bottom: BorderSide(
                                      width: 1,
                                      color: kSecondaryColor,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                child: Column(
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 30),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Vencimiento:",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                            ),
                                          ),
                                          Text(
                                            DateFormat('dd/MM/y')
                                                .format(date_payment),
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 30),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Ultimo Pago:",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                            ),
                                          ),
                                          Text(
                                            last_payment,
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 10),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Valor Mensual:",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                            ),
                                          ),
                                          Text(
                                            '\$ ' + monthly_value.toString(),
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),

                                    // Container(
                                    //   child: Align(
                                    //     alignment: Alignment.center,
                                    //     child: GestureDetector(
                                    //       onTap: () {
                                    //         print('prin');
                                    //       },
                                    //       child: Text(
                                    //         'Pagar',
                                    //         style: TextStyle(
                                    //           color: Colors.white,
                                    //           fontSize: 18,
                                    //           fontWeight: FontWeight.bold,
                                    //         ),
                                    //       ),
                                    //     ),
                                    //   ),
                                    //   decoration: BoxDecoration(
                                    //     shape: BoxShape.rectangle,
                                    //     color: kInfoColor,
                                    //     borderRadius: BorderRadius.only(
                                    //       topRight: Radius.circular(15),
                                    //       bottomLeft: Radius.circular(15),
                                    //     ),
                                    //   ),
                                    // ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        child: GestureDetector(
                          onTap: () {
                            mercadoPago();
                          },
                          child: loading
                              ? SizedBox(
                                  height: 10,
                                  width: 10,
                                  child: CircularProgressIndicator(
                                    valueColor:
                                        AlwaysStoppedAnimation(Colors.white),
                                  ),
                                )
                              : Container(
                                  padding: EdgeInsets.all(15),
                                  child:
                                      // Row(
                                      //         mainAxisAlignment:
                                      //             MainAxisAlignment.center,
                                      //         children: [
                                      //           Column(
                                      //             mainAxisAlignment:
                                      //                 MainAxisAlignment
                                      //                     .center,
                                      //             children: [
                                      //               Padding(
                                      //                 padding:
                                      //                     const EdgeInsets
                                      //                         .all(2),
                                      //                 child: SizedBox(
                                      //                   height: 10,
                                      //                   width: 10,
                                      //                   child:
                                      //                       CircularProgressIndicator(
                                      //                     valueColor:
                                      //                         AlwaysStoppedAnimation(
                                      //                             Colors
                                      //                                 .white),
                                      //                   ),
                                      //                 ),
                                      //               ),
                                      //             ],
                                      //           ),
                                      //           Column(
                                      //             mainAxisAlignment:
                                      //                 MainAxisAlignment
                                      //                     .center,
                                      //             children: [
                                      //               Text(
                                      //                 "Entrar",
                                      //                 style: TextStyle(
                                      //                   fontSize: 18,
                                      //                   color: Colors.white,
                                      //                   fontWeight:
                                      //                       FontWeight.bold,
                                      //                 ),
                                      //               ),
                                      //             ],
                                      //           ),
                                      //         ],
                                      //       )
                                      // :
                                      Center(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Flexible(
                                          fit: FlexFit.tight,
                                          flex: 3,
                                          child: Align(
                                            alignment: Alignment.centerLeft,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                right: 10,
                                              ),
                                              child: Image.asset(
                                                "assets/images/mercado-pago-logo-p.png",
                                                height: 20,
                                                // width: 20,r
                                              ),
                                            ),
                                          ),
                                        ),
                                        Flexible(
                                          fit: FlexFit.tight,
                                          flex: 4,
                                          child: Text(
                                            "Pagar",
                                            style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
          },
        );
      }),
    );
  }
}
