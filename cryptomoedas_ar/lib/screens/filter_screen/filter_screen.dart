import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/screens/components/app_bar_icon_simple.dart';
import 'package:cryptomoedas_ar/screens/components/app_bar_pages.dart';
import 'package:cryptomoedas_ar/screens/filter_screen/components/body.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FilterScreen extends StatefulWidget {
  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  String titlePage = 'Filtro';
  String nameColum1 = 'Ganancia';
  String nameColum2 = '%  ';
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      backgroundColor: kPrimaryColor,
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0,
        centerTitle: true,
        title: AppBarIconSimple(),
      ),
      body: BodyTablePages(
        titlePage: titlePage,
        nameColum1: nameColum1,
        nameColum2: nameColum2,
      ),
    );
  }
}
