import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cryptomoedas_ar/API/provedor.dart';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/models/list_provedores.dart';
import 'package:cryptomoedas_ar/models/user_manager.dart';
import 'package:cryptomoedas_ar/screens/rulos_screen/components/list_rulos.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

class BodyTablePages extends StatefulWidget {
  const BodyTablePages({
    Key key,
    @required this.titlePage,
    @required this.nameColum1,
    @required this.nameColum2,
    // @required this.provedor,
    // @required this.loading,
  }) : super(key: key);

  final String titlePage;
  final String nameColum1;
  final String nameColum2;

  @override
  _BodyTablePagesState createState() => _BodyTablePagesState();
}

class _BodyTablePagesState extends State<BodyTablePages> {
  bool _switchVal = true;

  //List<bool> listProvedores;
  //bool _checkBoxVal;

  var buenbit = true;
  var decrypto = true;
  var qubit = true;
  var ripio = true;
  var ripioExchange = true;
  var satoshiTango = true;
  var seSocio = true;

  bool notification = true;
  var price_notification;
  var gain_notification;

  final Firestore firestore = Firestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;
  //var listProvider;
  _BodyTablePagesState() {
    super.initState();
    getExchange();
  }

  Future getExchange() async {
    final FirebaseUser currentUser = await auth.currentUser();
    final DocumentSnapshot user =
        await firestore.collection('users').document(currentUser.uid).get();

    // notification
    notification = user.data['filter_notification'];
    price_notification = user.data['filter_price_notification'].toString();

    //  is int
    //     ? (user.data['filter_price_notification'] as int).toDouble()
    //     : user.data['filter_price_notification'];
    gain_notification = user.data['filter_gain_notification'].toString();
    // is int
    //     ? (user.data['filter_gain_notification'] as int).toDouble()
    //     : user.data['filter_gain_notification'];

    buenbit = user.data['providers_buenbit'];
    decrypto = user.data['providers_decrypto'];
    qubit = user.data['providers_qubit'];
    ripio = user.data['providers_ripio'];
    ripioExchange = user.data['providers_ripioExchange'];
    satoshiTango = user.data['providers_satoshiTango'];
    seSocio = user.data['providers_seSocio'];

    // listProvider = user.data['providers']
    //     .map((model) => ListProvedores.fromMap(model))
    //     .toList();

    // buenbit = listProvider[0].value;
    // decrypto = listProvider[1].value;
    // qubit = listProvider[2].value;
    // ripio = listProvider[3].value;
    // ripioExchange = listProvider[4].value;
    // satoshiTango = listProvider[5].value;
    // seSocio = listProvider[6].value;

    // return listProvider;
  }

  // var providers = getExchange();

  //provedor = {}
  @override
  Widget build(BuildContext context) {
    return Consumer<UserManager>(builder: (_, userManager, __) {
      return FutureBuilder(
        future: getExchange(),
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: SizedBox(
                height: 50,
                width: 50,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.white),
                ),
              ),
            );
          } else {
            return Container(
              height: double.infinity,
              width: double.infinity,
              child: ListView(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                widget.titlePage,
                                style: TextStyle(
                                    fontSize: 30, color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(bottom: 20),
                                padding: EdgeInsets.only(bottom: 8),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Notificaciones',
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                        //fontWeight: FontWeight.bold,
                                        fontFamily: 'PoiretOne',
                                      ),
                                    ),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                  border: Border(
                                    bottom: BorderSide(
                                      width: 1,
                                      color: kSecondaryColor,
                                    ),
                                  ),
                                ),
                              ),
                              Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Recibir Notificación:",
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                        Switch(
                                          value: this.notification,
                                          onChanged: (bool value) {
                                            setState(() =>
                                                this.notification = value);
                                            userManager.saveNotify(value);
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Text(
                                            "Avisame cuando la venta llegue arriba de (\$):",
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        SizedBox(
                                          height: 40,
                                          width: 100,
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                left: 10,
                                                right: 10,
                                                bottom: 1,
                                                top: 0),
                                            child: TextFormField(
                                              initialValue: price_notification,
                                              enabled: this.notification,
                                              keyboardType:
                                                  TextInputType.number,
                                              decoration: InputDecoration(
                                                  hintText: 'Ej.: 120.30'),
                                              autocorrect: false,
                                              onChanged: (value) {
                                                userManager.saveSell(value);
                                              },
                                            ),
                                            decoration: BoxDecoration(
                                              shape: BoxShape.rectangle,
                                              color: this.notification
                                                  ? Colors.white
                                                  : kSecondaryColor,
                                              borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(15),
                                                bottomLeft: Radius.circular(15),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Text(
                                            "Avisame cuando la ganancia llegue arriba de (\$):",
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        SizedBox(
                                          height: 40,
                                          width: 125,
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                left: 10,
                                                right: 10,
                                                bottom: 1,
                                                top: 0),
                                            child: TextFormField(
                                              initialValue:
                                                  gain_notification.toString(),
                                              keyboardType:
                                                  TextInputType.number,
                                              decoration: InputDecoration(
                                                  hintText: 'Ej.: 4352.30'),
                                              autocorrect: false,
                                              enabled: this.notification,
                                              onChanged: (value) {
                                                userManager.saveGain(value);
                                              },
                                            ),
                                            decoration: BoxDecoration(
                                              shape: BoxShape.rectangle,
                                              color: this.notification
                                                  ? Colors.white
                                                  : kSecondaryColor,
                                              borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(15),
                                                bottomLeft: Radius.circular(15),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Text(
                                            "Solo avisame sobre las Exchanges selecionadas:",
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Flexible(
                                          //  width: 200,
                                          child: SizedBox(
                                            height: 50,
                                            child: GestureDetector(
                                              child: Container(
                                                alignment: Alignment.center,
                                                //color: kInfoColor,
                                                padding: EdgeInsets.only(
                                                    left: 10,
                                                    right: 10,
                                                    bottom: 1,
                                                    top: 0),
                                                child: Text(
                                                  "Selecione las Exchanges",
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.rectangle,
                                                  color: kInfoColor,
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    topRight:
                                                        Radius.circular(15),
                                                    bottomLeft:
                                                        Radius.circular(15),
                                                  ),
                                                ),
                                              ),
                                              onTap: () {
                                                showDialog(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) =>
                                                          StatefulBuilder(
                                                    builder:
                                                        (context, setState) {
                                                      return SimpleDialog(
                                                        title:
                                                            Text('Exchanges'),
                                                        children: [
                                                          ListTile(
                                                            leading: Checkbox(
                                                              tristate: false,
                                                              onChanged:
                                                                  (bool value) {
                                                                setState(() =>
                                                                    this.buenbit =
                                                                        value);
                                                                userManager
                                                                    .saveProviders(
                                                                        'providers_buenbit',
                                                                        value);
                                                              },
                                                              value:
                                                                  this.buenbit,
                                                            ),
                                                            title:
                                                                Text("Buenbit"),
                                                            // onTap: () => Navigator.pop(
                                                            //     context, 'Buenbit'),
                                                          ),
                                                          ListTile(
                                                            leading: Checkbox(
                                                              tristate: false,
                                                              onChanged:
                                                                  (bool value) {
                                                                setState(() =>
                                                                    this.decrypto =
                                                                        value);
                                                                userManager
                                                                    .saveProviders(
                                                                        'providers_decrypto',
                                                                        value);
                                                              },
                                                              value:
                                                                  this.decrypto,
                                                            ),
                                                            title: Text(
                                                                "Decrypto"), // ${(listProvedores[0])
                                                            // onTap: () => Navigator.pop(
                                                            //     context, 'Decrypto'),
                                                          ),
                                                          ListTile(
                                                            leading: Checkbox(
                                                              tristate: false,
                                                              onChanged:
                                                                  (bool value) {
                                                                setState(() =>
                                                                    this.qubit =
                                                                        value);
                                                                userManager
                                                                    .saveProviders(
                                                                        'providers_qubit',
                                                                        value);
                                                              },
                                                              value: this.qubit,
                                                            ),
                                                            title: Text(
                                                                "Qubit Brokers"),
                                                            // onTap: () => Navigator.pop(
                                                            //     context,
                                                            //     'Qubit Brokers'),
                                                          ),
                                                          ListTile(
                                                            leading: Checkbox(
                                                              tristate: false,
                                                              onChanged:
                                                                  (bool value) {
                                                                setState(() =>
                                                                    this.ripio =
                                                                        value);
                                                                userManager
                                                                    .saveProviders(
                                                                        'providers_ripio',
                                                                        value);
                                                              },
                                                              value: this.ripio,
                                                            ),
                                                            title:
                                                                Text("Ripio"),
                                                            // onTap: () => Navigator.pop(
                                                            //     context, 'Ripio'),
                                                          ),
                                                          ListTile(
                                                            leading: Checkbox(
                                                              tristate: false,
                                                              onChanged:
                                                                  (bool value) {
                                                                setState(() =>
                                                                    this.ripioExchange =
                                                                        value);
                                                                userManager
                                                                    .saveProviders(
                                                                        'providers_ripioExchange',
                                                                        value);
                                                              },
                                                              value: this
                                                                  .ripioExchange,
                                                            ),
                                                            title: Text(
                                                                "Ripio Exchange"),
                                                            // onTap: () => Navigator.pop(
                                                            //     context,
                                                            //     'Ripio Exchange'),
                                                          ),
                                                          ListTile(
                                                            leading: Checkbox(
                                                              tristate: false,
                                                              onChanged:
                                                                  (bool value) {
                                                                setState(() =>
                                                                    this.satoshiTango =
                                                                        value);
                                                                userManager
                                                                    .saveProviders(
                                                                        'providers_satoshiTango',
                                                                        value);
                                                              },
                                                              value: this
                                                                  .satoshiTango,
                                                            ),
                                                            title: Text(
                                                                "SatoshiTango"),
                                                            // onTap: () => Navigator.pop(
                                                            //     context,
                                                            //     'SatoshiTango'),
                                                          ),
                                                          ListTile(
                                                            leading: Checkbox(
                                                              tristate: false,
                                                              onChanged:
                                                                  (bool value) {
                                                                setState(() =>
                                                                    this.seSocio =
                                                                        value);
                                                                userManager
                                                                    .saveProviders(
                                                                        'providers_seSocio',
                                                                        value);
                                                              },
                                                              value:
                                                                  this.seSocio,
                                                            ),
                                                            title:
                                                                Text("SeSocio"),
                                                            // onTap: () => Navigator.pop(
                                                            //     context, 'SeSocio'),
                                                          ),
                                                        ],
                                                      );
                                                    },
                                                  ),
                                                ).then((returnVal) {
                                                  if (returnVal != null) {
                                                    Scaffold.of(context)
                                                        .showSnackBar(
                                                      SnackBar(
                                                        content: Text(
                                                            "Clicou: $returnVal"),
                                                        action: SnackBarAction(
                                                          label: 'Ok',
                                                          onPressed: () {},
                                                        ),
                                                      ),
                                                    );
                                                  }
                                                });
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }
        },
      );
    });
  }
}
