import 'package:cryptomoedas_ar/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppBarIconSimple extends StatelessWidget {
  const AppBarIconSimple({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 70,
      child: Container(
        //color: kGreenColor,
        width: 50,
        //margin: EdgeInsets.all(16),
        alignment: Alignment.topCenter,
        child: SvgPicture.asset(
          "assets/images/logo_dypto.svg",
        ),
      ),
    );
  }
}
