import 'dart:convert';

import 'package:cryptomoedas_ar/API/api_plus_cambio.dart';
import 'package:cryptomoedas_ar/API/quotation.dart';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppBarPages extends StatefulWidget {
  @override
  _AppBarPagesState createState() => _AppBarPagesState();
}

class _AppBarPagesState extends State<AppBarPages> {
  var quotation = List<QuotationDolar>();
  double dolar = 0;
  _getDollarOficial() {
    API_Dolar.getDollarOficial().then((response) {
      setState(() {
        Iterable lista = json.decode(response.body);
        quotation =
            lista.map((model) => QuotationDolar.fromJson(model)).toList();
        // print(dolar.Type());
        dolar = double.parse(quotation[0].sell) * 1.3;

        //loading = false;
      });
    });
  }

//  _AppBarPagesState({
//     Key key,
//   }) : super(key: key);
  _AppBarPagesState() {
    super.initState();
    _getDollarOficial();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      //crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          height: 70,
          child: Container(
            //color: kGreenColor,
            width: 50,
            //margin: EdgeInsets.all(16),
            alignment: Alignment.topCenter,
            child: SvgPicture.asset(
              "assets/images/logo_dypto.svg",
            ),
          ),
        ),
        Expanded(
          child: Container(
            //color: kGreenColor,
            //margin: EdgeInsets.only(right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "Cotacion Dolar Oficial(+impuestos): ",
                  style: TextStyle(fontSize: 10, color: kSecondaryColor),
                ),
                Text(
                  dolar == 0 ? 'Cargando...' : dolar.toStringAsPrecision(4),
                  style: TextStyle(fontSize: 12, color: kSecondaryColor),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
