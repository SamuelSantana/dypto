import 'package:cryptomoedas_ar/components/helpers/validator.dart';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/models/user.dart';
import 'package:cryptomoedas_ar/models/user_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class SignupScreen extends StatelessWidget {
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  final User user = User();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: kPrimaryColor,
      // appBar: AppBar(
      //   title: Text("Criar conta"),
      //   centerTitle: true,
      // ),
      body: Center(child: Consumer<UserManager>(
        builder: (_, UserManager, __) {
          return ListView(
            padding: EdgeInsets.symmetric(horizontal: 20),
            shrinkWrap: true,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 20, top: 5),
                padding: EdgeInsets.all(5),
                height: 80,
                child: SvgPicture.asset(
                  "assets/images/logo_dypto.svg",
                ),
              ),
              Container(
                child: Form(
                  key: formkey,
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            left: 10, right: 10, bottom: 1, top: 0),
                        child: TextFormField(
                          textCapitalization: TextCapitalization.words,
                          enabled: !UserManager.loading,
                          decoration:
                              InputDecoration(hintText: 'Nombre Completo'),
                          validator: (name) {
                            if (name.isEmpty)
                              return "Campo Obligatorio";
                            else if (name.trim().split(' ').length <= 1)
                              return 'Digite el Nombre Completo';
                            return null;
                          },
                          onSaved: (name) => user.name = name,
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(15),
                            bottomLeft: Radius.circular(15),
                          ),
                        ),
                      ),
                      SizedBox(height: 25),
                      Container(
                        padding: EdgeInsets.only(
                            left: 10, right: 10, bottom: 1, top: 0),
                        child: TextFormField(
                          enabled: !UserManager.loading,
                          decoration: InputDecoration(hintText: 'DNI'),
                          validator: (dni) {
                            if (dni.isEmpty)
                              return "Campo Obligatorio";
                            else if (dni.length < 8)
                              return "Digite un DNI válido";

                            return null;
                          },
                          onSaved: (dni) => user.dni = dni,
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(15),
                            bottomLeft: Radius.circular(15),
                          ),
                        ),
                      ),
                      SizedBox(height: 25),
                      Container(
                        padding: EdgeInsets.only(
                            left: 10, right: 10, bottom: 1, top: 0),
                        child: TextFormField(
                          enabled: !UserManager.loading,
                          decoration: InputDecoration(hintText: 'E-mail'),
                          keyboardType: TextInputType.emailAddress,
                          validator: (email) {
                            if (email.isEmpty)
                              return "Campo Obligatorio";
                            else if (!emailValid(email))
                              return 'Email Incorrecto';
                            return null;
                          },
                          onSaved: (email) => user.email = email,
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(15),
                            bottomLeft: Radius.circular(15),
                          ),
                        ),
                      ),
                      SizedBox(height: 25),
                      Container(
                        padding: EdgeInsets.only(
                            left: 10, right: 10, bottom: 1, top: 0),
                        child: TextFormField(
                          enabled: !UserManager.loading,
                          decoration: InputDecoration(hintText: 'Contraseña'),
                          obscureText: true,
                          validator: (pass) {
                            if (pass.isEmpty)
                              return "Campo obligatório";
                            else if (pass.length < 4) return "Contraseña Corta";
                            return null;
                          },
                          onSaved: (pass) => user.password = pass,
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(15),
                            bottomLeft: Radius.circular(15),
                          ),
                        ),
                      ),
                      SizedBox(height: 25),
                      Container(
                        padding: EdgeInsets.only(
                            left: 10, right: 10, bottom: 1, top: 0),
                        child: TextFormField(
                          enabled: !UserManager.loading,
                          decoration:
                              InputDecoration(hintText: 'Contraseña otra vez'),
                          obscureText: true,
                          onSaved: (pass) => user.confirmPassword = pass,
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(15),
                            bottomLeft: Radius.circular(15),
                          ),
                        ),
                      ),
                      SizedBox(height: 25),
                      GestureDetector(
                        onTap: () {
                          if (formkey.currentState.validate()) {
                            formkey.currentState.save();

                            if (user.password != user.confirmPassword) {
                              scaffoldKey.currentState.showSnackBar(
                                SnackBar(
                                  content: Text("Contraseñas Diferentes"),
                                  backgroundColor: Colors.red,
                                ),
                              );
                              return;
                            }
                            UserManager.signup(
                              user: user,
                              onSuccess: () {
                                Navigator.of(context).pop();
                              },
                              onFail: (e) {
                                scaffoldKey.currentState.showSnackBar(
                                  SnackBar(
                                    content: Text("Error al cadastrar: $e"),
                                    backgroundColor: kTreeColor,
                                  ),
                                );
                              },
                            );
                          }
                        },
                        //color: kInfoColor,
                        // disabledColor: kSecondaryColor,
                        //textColor: Colors.white,
                        child: Container(
                          padding: EdgeInsets.all(15),
                          child: UserManager.loading
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(2),
                                          child: SizedBox(
                                            height: 15,
                                            width: 15,
                                            child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation(
                                                      Colors.white),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          " Registrando ...",
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                )
                              : Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "Registrarse",
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: kInfoColor,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(15),
                              bottomLeft: Radius.circular(15),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 35, bottom: 10),
                            child: Row(
                              children: [
                                Text(
                                  "¡Ya tengo una cuenta!",
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                      // Navigator.of(context)
                                      //     .popAndPushNamed('/login');
                                    },
                                    //padding: EdgeInsets.zero,
                                    child: Text(
                                      " Login",
                                      style: TextStyle(
                                        color: kSecondaryColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      )),
    );
  }
}
