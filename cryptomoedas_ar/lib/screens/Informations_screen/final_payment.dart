import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/screens/components/app_bar_icon_simple.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FinalPaymentScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      backgroundColor: kPrimaryColor,
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0,
        centerTitle: true,

        //title: AppBarIconSimple(),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              height: 50,
              child: Container(
                //color: kGreenColor,
                //width: 50,
                //margin: EdgeInsets.all(16),
                alignment: Alignment.topCenter,
                child: SvgPicture.asset(
                  "assets/icons/icon_ok.svg",
                ),
              ),
            ),
            SizedBox(
              height: 100,
              child: Container(
                //color: kGreenColor,
                //width: 50,
                //margin: EdgeInsets.all(16),
                alignment: Alignment.topCenter,
                child: SvgPicture.asset(
                  "assets/images/logo_dypto.svg",
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Text(
                    'Transacción',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    'Realizada con exito',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                        //Navigator.of(context).pushReplacementNamed('/home');
                      },
                      child: Container(
                        padding: EdgeInsets.all(15),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            "Volver al Inicio",
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: kInfoColor,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(15),
                            bottomLeft: Radius.circular(15),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }
}
