import 'dart:async';
import 'dart:convert';
import 'package:cryptomoedas_ar/API/api.dart';
import 'package:cryptomoedas_ar/API/api_plus_cambio.dart';
import 'package:cryptomoedas_ar/API/provedor.dart';
import 'package:cryptomoedas_ar/API/quotation.dart';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/models/rulos.dart';
import 'package:cryptomoedas_ar/models/user_manager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListRulos extends StatefulWidget {
  @override
  _ListRulosState createState() => _ListRulosState();
}

class _ListRulosState extends State<ListRulos> {
  var provedorUSD = List<Provedor>();
  var provedorARS = List<Provedor>();
  var quotation = List<QuotationDolar>();
  double dolar = 0;
  Timer timer;
  var listRulos = List<Rulo>();
  var rulo = Rulo();
  var loading = false;

  _getDaiRulos() {
    API_Dolar.getDollarOficial().then((response) {
      Iterable lista = json.decode(response.body);
      quotation = lista.map((model) => QuotationDolar.fromJson(model)).toList();
      dolar = double.parse(quotation[0].sell) * 1.3;
    });

    API.getDaiUsdAsk().then((response) {
      Iterable lista = json.decode(response.body);
      provedorUSD = lista.map((model) => Provedor.fromJson(model)).toList();

      API.getDaiBid().then((response) {
        setState(() {
          listRulos = [];
          Iterable lista = json.decode(response.body);
          //loading = true;
          provedorARS = lista.map((model) => Provedor.fromJson(model)).toList();

          // passar as variáveis pelos filtros de cada cliente
          provedorUSD.forEach((usd) {
            provedorARS.forEach((ars) {
              var totalInvest = (200 * dolar);
              var totalSell =
                  (((200 / usd.fields.usd.ask).toInt()) * ars.fields.ars.bid);
              var gain = totalSell - totalInvest;
              var percent = (gain * 100) / totalInvest;

              rulo = Rulo(
                  name_ask: usd.fields.name,
                  ask: usd.fields.usd.ask,
                  bid: ars.fields.ars.bid,
                  name_bid: ars.fields.name,
                  percent: percent,
                  gain: gain);
              listRulos.add(rulo);
            });
          });

          listRulos.sort((a, b) => b.gain.compareTo(a.gain));

          //rulos = loading = false;
        });
      });
    });
  }

  _ListRulosState() {
    super.initState();
    _getDaiRulos();
    timer = Timer.periodic(Duration(seconds: 55), (Timer t) => _getDaiRulos());
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var tim = DateTime.now();
    return FutureBuilder(
        future: API.getDaiUsdAsk(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              alignment: Alignment.centerLeft,
              height: MediaQuery.of(context).size.height * 0.6,
              // padding: EdgeInsets.only(
              //     left: 10, right: 10, bottom: 1, top: 0),
              child: Consumer<UserManager>(
                builder: (_, userManager, __) {
                  return ListView.builder(
                    itemCount: listRulos.length,
                    itemBuilder: (context, index) {
                      return loading
                          ? SizedBox(
                              height: 10,
                              width: 10,
                              child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation(kPrimaryColor),
                              ),
                            )
                          : Container(
                              margin: EdgeInsets.symmetric(vertical: 10),
                              child: ListTile(
                                title: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      '\$ ' +
                                          listRulos[index]
                                              .gain
                                              .toStringAsPrecision(6),
                                      style: TextStyle(
                                          fontSize: 20, color: kInfoColor),
                                    ),
                                    Text(
                                      listRulos[index]
                                              .percent
                                              .toStringAsPrecision(4) +
                                          ' %',
                                      style: TextStyle(
                                          fontSize: 18, color: kGreenColor),
                                    ),
                                  ],
                                ),
                                subtitle: Column(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(bottom: 3, top: 2),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Comprás en:',
                                            style: TextStyle(
                                                fontSize: 11,
                                                color: kSecondaryColor),
                                          ),
                                          Text(
                                            listRulos[index].name_ask,
                                            style: TextStyle(
                                                fontSize: 11,
                                                color: kSecondaryColor),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Vendés en:',
                                          style: TextStyle(
                                              fontSize: 11,
                                              color: kSecondaryColor),
                                        ),
                                        Text(
                                          listRulos[index].name_bid,
                                          style: TextStyle(
                                              fontSize: 11,
                                              color: kSecondaryColor),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            );
                    },
                  );

                  //   ],
                  // );
                },
              ),
            );
          } else if (snapshot.hasError) {
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 50),
              child: Center(
                child: Card(
                  color: kTreeColor,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text(
                          "No fue posible cargar los datos.",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "Verificá tu conexión !",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
          return Center(
            child: Padding(
              padding: const EdgeInsets.all(40),
              child: SizedBox(
                height: 40,
                width: 40,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(kPrimaryColor),
                ),
              ),
            ),
          );
        });
  }
}
