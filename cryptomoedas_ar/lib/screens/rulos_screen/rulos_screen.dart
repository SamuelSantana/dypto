import 'dart:async';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/screens/components/app_bar_pages.dart';
import 'package:cryptomoedas_ar/screens/rulos_screen/components/body.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RulosScreen extends StatelessWidget {
  String titlePage = 'Mejores Rulos';
  String nameColum1 = 'Ganancia';
  String nameColum2 = '%  ';

  Timer timer;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      backgroundColor: kPrimaryColor,
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0,
        centerTitle: false,
        title: AppBarPages(),
      ),
      body: BodyTablePages(
        titlePage: titlePage,
        nameColum1: nameColum1,
        nameColum2: nameColum2,
      ),
    );
  }
}
