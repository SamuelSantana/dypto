import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cryptomoedas_ar/constants.dart';
import 'package:cryptomoedas_ar/models/user_manager.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  UserManager userManager;
  final Firestore firestore = Firestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;

  void expiredPayment() async {
    DateTime dateNow = DateTime.now();
    final FirebaseUser currentUser = await auth.currentUser();
    if (currentUser == null) {
      Navigator.of(context).pushReplacementNamed('/login');
    } else {
      final DocumentSnapshot user =
          await firestore.collection('users').document(currentUser.uid).get();
      final DateTime paymentDate = user.data['payment_date'].toDate();

      if (dateNow.difference(paymentDate).inDays > 0) {
        // tem que colocar para a tela explicativa de vencimento do prazo.
        Navigator.of(context).pushReplacementNamed('/latepayment');
      } else {
        Navigator.of(context).pushReplacementNamed('/home');
      }
    }
  }

  _LoadingScreenState() {
    super.initState();
    expiredPayment();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: Center(
        child: Container(
          margin: EdgeInsets.only(bottom: 20, top: 5),
          padding: EdgeInsets.all(5),
          height: 100,
          child: SvgPicture.asset(
            "assets/images/logo_dypto.svg",
          ),
        ),
      ),
    );
  }
}
