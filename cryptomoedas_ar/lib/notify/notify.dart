import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class Notify extends StatefulWidget {
  @override
  _NotifyState createState() => _NotifyState();
}

class _NotifyState extends State<Notify> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    this.startFirebaseListeners();
  }

  void startFirebaseListeners() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('Mensagem: $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('Resume: $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('Launch: $message');
      },
    );

    _firebaseMessaging.getToken().then((token) {
      print("Token: $token");
      //Firestore.instance.collection('teste').add({'Token:': '$token'});
    });
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registred: $settings");
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container();
  }
}
