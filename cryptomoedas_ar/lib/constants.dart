import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF560089);
const kInfoColor = Color(0xFF33004F);
const kTreeColor = Color(0xFFB80082);
const kBlackColor = Color(0xFF222222);
const kWarningColor = Color(0xFFFFC61F);
const kRedColor = Color(0xFFC1292E);
const kGreenColor = Color(0xFF129B00);
const kSecondaryColor = Color(0xFFB5BFD0);
const kTextColor = Color(0xFF50505D);
const kTextLightColor = Color(0xFF6A727D);
