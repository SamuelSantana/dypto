String getErrosString(String code) {
  switch (code) {
    case 'ERROR_WEAK_PASSWORD':
      return 'Calidad de contraseña baja.';
    case 'ERROR_INVALID_EMAIL':
      return 'Su e-mail es inválido';
    case 'ERROR_EMAIL_ALREADY_IN_USE':
      return 'E-mail existente en ese aplicativo.';
    case 'ERROR_INVALID_CREDENTIAL':
      return 'Su e-mail es inválido';
    case 'ERROR_WRONG_PASSWORD':
      return 'Su contraseña esta Incorrecta';
    case 'ERROR_USER_NOT_FOUND':
      return 'No hay usuário con este e-mail';
    case 'ERROR_USER_DISABLED':
      return 'Este usuário fue desactivado';
    case 'ERROR_TOO_MANY_REQUESTS':
      return 'Muchos intentos. Intente mas tarde.';
    case 'ERROR_OPERATION_NOT_ALLOWED':
      return 'Operación no permitida';

    default:
      return 'Ocurió un error';
  }
}
