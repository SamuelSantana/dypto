class Provedor {
  bool _loading = false;
  bool get loading => _loading;

  Fields fields;

  Provedor({this.fields});

  Provedor.fromJson(Map<String, dynamic> json) {
    fields =
        json['fields'] != null ? new Fields.fromJson(json['fields']) : null;
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   if (this.fields != null) {
  //     data['fields'] = this.fields.toJson();
  //   }
  //   return data;
  // }
}

class Fields {
  String name;
  String url;
  String icon;
  ARS ars;
  USD usd;

  Fields({this.name, this.url, this.icon, this.ars, this.usd});

  Fields.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
    icon = json['icon'];
    ars = json['ARS'] != null ? new ARS.fromJson(json['ARS']) : null;
    usd = json['USD'] != null ? new USD.fromJson(json['USD']) : null;
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['name'] = this.name;
  //   data['url'] = this.url;
  //   data['icon'] = this.icon;
  //   if (this.aRS != null) {
  //     data['ARS'] = this.aRS.toJson();
  //   }
  //   return data;
  // }
}

class ARS {
  double ask;
  double bid;
  double askWithoutFees;
  double bidWithoutFees;
  double variation;
  String formatted_bid;

  ARS(
      {this.ask,
      this.bid,
      this.askWithoutFees,
      this.bidWithoutFees,
      this.variation,
      this.formatted_bid});

  ARS.fromJson(Map<String, dynamic> json) {
    ask = json['ask'] is int ? (json['ask'] as int).toDouble() : json['ask'];
    bid = json['bid'] is int ? (json['bid'] as int).toDouble() : json['bid'];
    askWithoutFees = json['ask_whitout_fees'] is int
        ? (json['ask_whitout_fees'] as int).toDouble()
        : json['ask_whitout_fees'];
    bidWithoutFees = json['bid_without_fees'] is int
        ? (json['bid_without_fees'] as int).toDouble()
        : json['bid_without_fees'];
    variation = json['variation'] is int
        ? (json['variation'] as int).toDouble()
        : json['variation'];
    formatted_bid = json['formatted_bid'] as String;
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['ask'] = this.ask;
  //   data['bid'] = this.bid;
  //   data['ask_whitout_fees'] = this.askWhitoutFees;
  //   data['bid_without_fees'] = this.bidWithoutFees;
  //   data['variation'] = this.variation;
  //   data['formatted_ask'] = this.formattedAsk;
  //   return data;
  // }
}

class USD {
  double ask;
  double bid;
  double askWithoutFees;
  double bidWithoutFees;
  String formattedAsk;

  USD(
      {this.ask,
      this.bid,
      this.askWithoutFees,
      this.bidWithoutFees,
      this.formattedAsk});

  USD.fromJson(Map<String, dynamic> json) {
    // ask = json['ask'];
    // bid = json['bid'];
    // askWithoutFees = json['ask_without_fees'];
    // bidWithoutFees = json['bid_without_fees'];
    // formattedAsk = json['formatted_ask'];
    ask = json['ask'] is int ? (json['ask'] as int).toDouble() : json['ask'];
    bid = json['bid'] is int ? (json['bid'] as int).toDouble() : json['bid'];
    askWithoutFees = json['ask_whitout_fees'] is int
        ? (json['ask_whitout_fees'] as int).toDouble()
        : json['ask_whitout_fees'];
    bidWithoutFees = json['bid_without_fees'] is int
        ? (json['bid_without_fees'] as int).toDouble()
        : json['bid_without_fees'];
    formattedAsk = json['formatted_ask'] as String;
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['ask'] = this.ask;
  //   data['bid'] = this.bid;
  //   data['ask_without_fees'] = this.askWithoutFees;
  //   data['bid_without_fees'] = this.bidWithoutFees;
  //   data['formatted_ask'] = this.formattedAsk;
  //   return data;
  // }
}
