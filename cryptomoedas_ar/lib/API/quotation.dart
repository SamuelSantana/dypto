class QuotationDolar {
  String sell;
  double sellVariation;

  QuotationDolar({this.sell, this.sellVariation});

  QuotationDolar.fromJson(Map<String, dynamic> json) {
    sell = json['sell'].toString();
    // json['sell'] is int ? (json['sell'] as int).toDouble() : json['sell'];
    sellVariation = json['sell_variation'] is int
        ? (json['sell_variation'] as int).toDouble()
        : json['sell_variation'];
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['sell'] = this.sell;
  //   data['sell_variation'] = this.sellVariation;
  //   return data;
  // }
}
