import 'package:http/http.dart' as http;

const baseUrl = 'https://api.cryptosaurio.com/prices';

class API {
  static Future getDaiBid() async {
    var url = baseUrl + "/ARS/dai/bid";
    return await http.get(url);
  }

  static Future getDaiAsk() async {
    var url = baseUrl + "/ARS/dai/ask";
    return await http.get(url);
  }

  static Future getDaiUsdAsk() async {
    var url = baseUrl + "/USD/dai/ask";
    return await http.get(url);
  }
}
