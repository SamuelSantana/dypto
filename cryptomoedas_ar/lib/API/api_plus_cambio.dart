import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

const baseUrl = 'https://api.pluscambio.com.ar';

class API_Dolar {
  static Future getDollarOficial() async {
    var url = baseUrl + "/currencies?front-web=true";
    return await http.get(url);
  }
}
