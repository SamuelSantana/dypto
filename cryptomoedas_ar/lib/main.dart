import 'package:cryptomoedas_ar/screens/Informations_screen/final_payment.dart';
import 'package:cryptomoedas_ar/screens/Informations_screen/late_payment.dart';
import 'package:cryptomoedas_ar/screens/compras_screen/compras_screen.dart';
import 'package:cryptomoedas_ar/screens/filter_screen/filter_screen.dart';
import 'package:cryptomoedas_ar/screens/forgot_password/forgot_password.dart';
import 'package:cryptomoedas_ar/screens/home_screen/home_screen.dart';
import 'package:cryptomoedas_ar/models/user_manager.dart';
import 'package:cryptomoedas_ar/screens/loading_screen/loading_screen.dart';
import 'package:cryptomoedas_ar/screens/login-screen/login_screen.dart';
import 'package:cryptomoedas_ar/screens/perfil_screen/perfil_screen.dart';
import 'package:cryptomoedas_ar/screens/rulos_screen/rulos_screen.dart';
import 'package:cryptomoedas_ar/screens/sell_screen/sell_screen.dart';
import 'package:cryptomoedas_ar/screens/signup-screen/signup_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'screens/payment_screen/payment_screen.dart';

void main() {
  runApp(CryptoMoedas());
  // Firestore.instance.collection('teste').add({'teste:': 'teste'});
}

class CryptoMoedas extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => UserManager(),
      lazy: false,
      child: MaterialApp(
        title: 'Dypto main',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          fontFamily: 'MontserratAlternates',
        ),
        initialRoute: '', //UserManager().isLoggedin ? '/home' : '/login',
        onGenerateRoute: (settings) {
          switch (settings.name) {
            case '/signup':
              return MaterialPageRoute(
                builder: (_) => SignupScreen(),
              );
            case '/login':
              return MaterialPageRoute(
                builder: (_) => LoginScreen(),
              );
            case '/home':
              return MaterialPageRoute(
                builder: (_) => HomeScreen(),
              );
            case '/compras':
              return MaterialPageRoute(
                builder: (_) => ComprasScreen(),
              );
            case '/sell':
              return MaterialPageRoute(
                builder: (_) => SellScreen(),
              );
            case '/rulos':
              return MaterialPageRoute(
                builder: (_) => RulosScreen(),
              );
            case '/filter':
              return MaterialPageRoute(
                builder: (_) => FilterScreen(),
              );
            case '/payment':
              return MaterialPageRoute(
                builder: (_) => PaymentScreen(),
              );
            case '/perfil':
              return MaterialPageRoute(
                builder: (_) => PerfilScreen(),
              );
            case '/finalpayment':
              return MaterialPageRoute(
                builder: (_) => FinalPaymentScreen(),
              );
            case '/forgotPassword':
              return MaterialPageRoute(
                builder: (_) => ForgotPasswordScreen(),
              );
            case '/latepayment':
              return MaterialPageRoute(
                builder: (_) => LatePaymentScreen(),
              );
            default:
              return MaterialPageRoute(
                builder: (_) => LoadingScreen(),
              );
          }
        },
        //home: HomeScreen(), //Notify(),,
      ),
    );
  }
}

// var client = http.Client();

// timeStamp() async {
//   try {
//     var uriResponse = await client.post('https://example.com/whatsit/create',
//         body: {'name': 'doodle', 'color': 'blue'});
//     print(await client.get(uriResponse.bodyFields['uri']));
//   } finally {
//     client.close();
//   }

//   final response =
//       await http.get('http://armariosinteligentes.com/api/v3/timestamp');

//   if (response.statusCode == 200) {
//     // If server returns an OK response, parse the JSON.
//     var jsonResponse = json.decode(response.body);
//     tempoStamp tempo = new tempoStamp.fromJson(jsonResponse);
//     var time = ('${tempo.timestamp}');
//     return time;
//   } else {
//     // If that response was not OK, throw an error.
//     throw Exception('Failed to load post');
//   }
// }
//https://api.cryptosaurio.com/prices/ARS/dai/ask
// requisitionAPI() async {
//   final String crip = 'DAI';
//   final resp =
//       await http.get('https://api.cryptosaurio.com/prices/ARS/dai/bid');
//   if (resp.statusCode == 200) {
//     var jsonResponse = json.decode(resp.body)
//     print(resp.body);
//   }
// }
