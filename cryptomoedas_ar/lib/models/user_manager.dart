import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cryptomoedas_ar/components/helpers/firebase_errors.dart';
import 'package:cryptomoedas_ar/models/list_provedores.dart';
import 'package:cryptomoedas_ar/models/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class UserManager extends ChangeNotifier {
  UserManager() {
    _loadCurrentUser();
  }

  final FirebaseAuth auth = FirebaseAuth.instance;
  final Firestore firestore = Firestore.instance;

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  User user;

  bool _loading = false;
  bool get loading => _loading;
  bool get isLoggedin => user != null;

// ============= VERIFICAÇÃO DE VENCIMENTO  ========================
// Verifica se a data de pagamento acrescido de dois dias é maior que a data de hoje.
// Os dois dias é referente a data de compensação de pagamentos
  Future<bool> expiredPayment() async {
    DateTime date = DateTime.now();
    final FirebaseUser currentUser = await auth.currentUser();

    final DocumentSnapshot user =
        await firestore.collection('users').document(currentUser.uid).get();

    return user.data['payment_date'].add(Duration(days: 2)) <= date;
  }

// =====================  LOGIN  ==========================

  Future<void> signIn({User user, Function onFail, Function onSuccess}) async {
    loading = true;
    try {
      final AuthResult result = await auth.signInWithEmailAndPassword(
          email: user.email, password: user.password);

      await _loadCurrentUser(firebaseUser: result.user);

      onSuccess();
    } on PlatformException catch (e) {
      print(getErrosString(e.code));
      onFail(getErrosString(e.code));
    }

    loading = false;
  }

// ===============================  SIGN UP  =============================

  Future<void> signup({User user, Function onFail, Function onSuccess}) async {
    loading = true;

    try {
      final AuthResult result = await auth.createUserWithEmailAndPassword(
          email: user.email, password: user.password);

      //this.user = result.user;
      user.id = result.user.uid;
      this.user = user;

      await user.saveData();
      auth.signOut();
      user = null;
      onSuccess();
    } on PlatformException catch (e) {
      onFail(getErrosString(e.code));
    }

    loading = false;
  }

  // =============================== FORGOT PASSWORD  =============================

  Future<void> forgotPassword(
      {User user, Function onFail, Function onSuccess}) async {
    loading = true;

    try {
      await auth.sendPasswordResetEmail(email: user.email);

      onSuccess();
    } on PlatformException catch (e) {
      onFail(getErrosString(e.code));
    }

    loading = false;
  }

// ============================  SIGN OUT ========================================
  void signOut() {
    auth.signOut();
    user = null;
    notifyListeners();
  }

  // ============================== PROVEDORES ==============================]

  Future<List> getProviders() async {
    final FirebaseUser currentUser = await auth.currentUser();
    final DocumentSnapshot user =
        await firestore.collection('users').document(currentUser.uid).get();
    var listProvider = user.data['providers']
        .map((model) => ListProvedores.fromMap(model))
        .toList();
    print(listProvider[0]);
    return listProvider;
  }

  // Future<void> saveProviders(name, value) async {
  //   final FirebaseUser currentUser = await auth.currentUser();
  //   //final DocumentSnapshot user = await firestore
  //   Firestore.instance.collection('users').document(currentUser.uid).setData({
  //     'providers': [
  //       {'name': name, 'value': value}
  //     ]
  //   });
  //   //return user != null;
  // }
  Future<void> saveProviders(name, value) async {
    final FirebaseUser currentUser = await auth.currentUser();
    //final DocumentSnapshot user = await firestore
    Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .updateData({'${name}': value});
    //return user != null;
  }

  Future<void> saveNotify(value) async {
    final FirebaseUser currentUser = await auth.currentUser();
    Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .updateData({'filter_notification': value});
  }

  Future<void> saveSell(value) async {
    final FirebaseUser currentUser = await auth.currentUser();
    Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .updateData({'filter_price_notification': value});
  }

  Future<void> saveGain(value) async {
    final FirebaseUser currentUser = await auth.currentUser();
    Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .updateData({'filter_gain_notification': value});
  }

  set loading(bool value) {
    _loading = value;
    notifyListeners();
  }

  // DocumentReference get firestoreRef =>
  //     Firestore.instance.document('users/$id');

  // Future<void> saveToken(token) async {
  //   await firestoreRef.updateData(
  //       token); //Firestore.instance.collection('users').document(id).setData(toMap());
  // }

  Future<void> _loadCurrentUser({FirebaseUser firebaseUser}) async {
    final FirebaseUser currentUser = firebaseUser ?? await auth.currentUser();

    if (currentUser != null) {
      final DocumentSnapshot docUser =
          await firestore.collection('users').document(currentUser.uid).get();

      _firebaseMessaging.getToken().then((token) async {
        if (docUser.data['token'] != token) {
          await firestore
              .collection('users')
              .document(currentUser.uid)
              .updateData({'token': '$token'});
        }
      });
      User.fromDocument(docUser);
    }
    notifyListeners();
  }

  // ===================================  PAYMENT  ================================

  // getDuePayment() async {
  //   final FirebaseUser currentUser = await auth.currentUser();

  //   var detail_payment_user = await firestore
  //       .collection('users')
  //       .document(currentUser.uid)
  //       .snapshots()
  //       .map((document) => Payment.fromDocument(document.data['payment_date']))
  //       .toList();
  //   print(detail_payment_user);

  //   return detail_payment_user;
  // }

  // Future<String> getLastPayment() async {
  //   final FirebaseUser currentUser = await auth.currentUser();

  //   var detail_payment_user =
  //       await firestore.collection('users').document(currentUser.uid).get();
  //   return detail_payment_user.data['last_payment'];
  // }

  // Future<double> getMonthlyPayment() async {
  //   var value_payment = await firestore
  //       .collection('monthly_payment')
  //       .document('SUFDN3eWrTsfpFaDYiQp')
  //       .get();

  //   return double.parse(value_payment.data['unit_price']);
  // }
}
