import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cryptomoedas_ar/models/payment.dart';
import 'package:firebase_auth/firebase_auth.dart';

class PaymentManager {
  static Future getDuePayment() async {
    final Firestore firestore = Firestore.instance;
    final FirebaseAuth auth = FirebaseAuth.instance;
    final FirebaseUser currentUser = await auth.currentUser();

    var detail_payment_user = await firestore
        .collection('users')
        .document(currentUser.uid)
        .snapshots()
        .map((document) => Payment.fromDocument(document.data['payment_date']))
        .toList();
    print(detail_payment_user);

    return detail_payment_user;
  }
}
