import 'package:cloud_firestore/cloud_firestore.dart';

class ListProvedores {
  String name;
  bool value;
  //1final DocumentReference reference;

  ListProvedores({this.name, this.value});

  ListProvedores.fromMap(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  ListProvedores.fromSnapshot(DocumentSnapshot snapshot)
      : name = snapshot['name'],
        value = snapshot['value'];

  // @override
  // String toString() => "Record<$name: $value:>";
  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['name'] = this.name;
  //   data['value'] = this.value;
  // }
}
