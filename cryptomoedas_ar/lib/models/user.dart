import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  User({this.email, this.password, this.name, this.dni, this.id});

  User.fromDocument(DocumentSnapshot document) {
    id = document.documentID;
    name = document.data['name'] as String;
    email = document.data['email'] as String;
    dni = document.data['dni'] as String;
  }

  String id;
  String name;
  String dni;
  String email;
  String password;

  String confirmPassword;

  DocumentReference get firestoreRef =>
      Firestore.instance.document('users/$id');

  Future<void> saveData() async {
    await firestoreRef.setData(
        toMap()); //Firestore.instance.collection('users').document(id).setData(toMap());
  }

  Future<void> saveToken(token) async {
    await firestoreRef.updateData(
        token); //Firestore.instance.collection('users').document(id).setData(toMap());
  }

  Map<String, dynamic> toMap() {
    DateTime date = DateTime.now();
    return {
      'name': name,
      'email': email,
      'dni': dni,
      'last_payment': date, //date.add(Duration(days: 7)),
      'payment_date': date.add(Duration(days: 3)),
      'start_registration': DateTime.now(),
      'first_access': true,
      'token': '',
      'paid': false,
      'online': true,
      'last_notification': 0.0,
      'last_notification_time': DateTime.now(),
      'last_notification_gain': 0.0,
      'last_notification_time_gain': DateTime.now(),
      'filter_percentage_from': 0.0,
      'filter_price_notification': 0.0,
      'filter_gain_notification': 0.0,
      'filter_notification': false,
      'filter_coin': ['dai'],
      'providers_buenbit': true,
      'providers_decrypto': true,
      'providers_qubit': true,
      'providers_ripio': true,
      'providers_ripioExchange': true,
      'providers_satoshiTango': true,
      'providers_seSocio': true,
      'ars_usd': 'ARS',
    };
  }
}

class Providers {
  String name;
  bool value;

  Providers({this.name, this.value});

  Providers.fromJson(Map<String, dynamic> json) {
    name = json['name'].toString();
    value = json['value'] as bool;
  }
}
