class Rulo {
  String name_ask;
  String name_bid;
  double bid;
  double ask;
  double gain;
  double percent;

  Rulo({
    this.ask,
    this.bid,
    this.gain,
    this.name_ask,
    this.name_bid,
    this.percent,
  });
}
