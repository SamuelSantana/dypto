import 'package:cloud_firestore/cloud_firestore.dart';

class Payment {
  Payment.fromDocument(DocumentSnapshot document) {
    last_payment = document['last_payment'] as String;
    date_payment = document['date_payment'] as String;
  }

  String last_payment;
  String date_payment;
}
