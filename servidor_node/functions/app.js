const express = require('express')
const morgan = require('morgan');
const path = require('path');
const cron = require('node-cron');

const app = express();

const cryptosaurio = require('./src/routes/cryptosaurio');

// settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
//app.engine('');


// middlrewares
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));


//routes
//app.use(cryptosaurio);

cron.schedule('0 * * * * *', () => {
    console.log("oii")
    cryptosaurio();

})
//statics files
app.use(express.static(path.join(__dirname, 'public')));


///app.listen(app.get('port'));
//console.log("Server on port : ", app.get('port'));

module.exports = app