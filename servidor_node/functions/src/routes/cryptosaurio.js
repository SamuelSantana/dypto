const { Router } = require('express');
const router = Router();
const admin = require('firebase-admin');
const axios = require('axios')
const sendMsg = require('../lib/send_notify')
const dateTime = require('node-datetime')

var serviceAccount = require('../../criptomoedasar-firebase-adminsdk-3j4na-800c51c7e7.json');
const DateTime = require('node-datetime/src/datetime');
const e = require('express');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
})
const db = admin.firestore()



module.exports = async function enviaNotificacao(req, res) {
    const hostname = 'https://api.cryptosaurio.com/'
    const plusCambio = 'https://api.pluscambio.com.ar/currencies?front-web=true'
    const link_dai_ask = 'prices/USD/dai/ask' // PREÇO DE COMPRA
    const link_dai_bid = 'prices/ARS/dai/bid' // PREÇO DE VENDA
    const link_rolls = 'rolls/ARS'
    let dai_ask_usd;
    let dai_bid_ars;
    let dolar;
    let melhoresRulos = [];


    let output;

    // BUSCA DOS VALORES DE COMPRA EM DOLAR
    axios.get(hostname + link_dai_ask).then(function (res) {

        //se tiver um retorno do site
        if (res['status'] == 200) {
            dai_ask_usd = res.data.map(a => {
                return a = ({
                    'name': a['fields']['name'],
                    'ask': a['fields']['USD']['ask']
                })
            })

            // FAZ A BUSCA DE VALORES DE VENDA EM PESOS
            axios.get(hostname + link_dai_bid).then(function (res) {
                if (res['status'] == 200) {
                    dai_bid_ars = res.data.map(a => {
                        return a = ({
                            'name': a['fields']['name'],
                            'bid': a['fields']['ARS']['bid']
                        })
                    })

                    // puxa os dados da plus cambio
                    axios.get(plusCambio).then(function (res) {
                        if (res.status == 200) {
                            dolar = res.data[0].sell * 1.3;
                            console.log('Plus Cambio', res.data[0].sell * 1.3)

                            // FORMANDO A LISTA DE MELHORES RULOS

                            dai_ask_usd.forEach(usd => {
                                dai_bid_ars.forEach(ars => {
                                    var totalInvest = (200 * dolar);
                                    var totalSell =
                                        ((parseInt(200 / usd.ask)) * ars.bid);
                                    var gain = totalSell - totalInvest;
                                    var percent = (gain * 100) / totalInvest;

                                    var rulo = {
                                        'name_ask': usd.name,
                                        'ask': usd.ask,
                                        'name_bid': ars.name,
                                        'bid': ars.bid,
                                        'percent': percent.toFixed(2),
                                        'gain': parseFloat(gain.toFixed(2))
                                    };
                                    //console.log(rulo)
                                    melhoresRulos.push(rulo);
                                });
                            });

                            // console.log('Melhores Rulos: ', melhoresRulos)
                            console.log(melhoresRulos);

                            db.collection('users').get()
                                .then((snapshot) => {
                                    snapshot.forEach((doc) => {
                                        user = doc.data();

                                        //verificar se pode enviar a notificação
                                        if (user.filter_notification == true && user.payment_date.toDate() >= dateTime.create()['_now']) {
                                            console.log(user.name, 'Quer notificação')

                                            melhoresRulos_user = melhoresPrices_user = melhoresRulos
                                            // retirando o provider da lista de providers qubit do usuário 
                                            if (!user.providers_seSocio) {
                                                melhoresRulos_user = melhoresRulos_user.filter((elem) => {
                                                    return elem.name_bid != 'SeSocio' && elem.name_ask != 'SeSocio';
                                                })
                                                console.log(user.name, 'não quer sesocio')
                                            }
                                            // retirando o provider da lista de providers qubit do usuário 
                                            if (!user.providers_qubit) {
                                                melhoresRulos_user = melhoresRulos_user.filter((elem) => {
                                                    return elem.name_bid != 'Qubit Brokers' && elem.name_ask != 'Qubit Brokers';
                                                })
                                                console.log(user.name, 'não quer qubit')
                                            }
                                            // retirando o provider da lista de providers Satoshitango do usuário 
                                            if (!user.providers_satoshiTango) {
                                                melhoresRulos_user = melhoresRulos_user.filter((elem) => {
                                                    return elem.name_bid != 'Satoshitango' && elem.name_ask != 'Satoshitango';
                                                })
                                                console.log(user.name, 'não quer santiago')
                                            }
                                            // retirando o provider da lista de providers Decrypto do usuário 
                                            if (!user.providers_decrypto) {
                                                melhoresRulos_user = melhoresRulos_user.filter((elem) => {
                                                    return elem.name_bid != 'Decrypto' && elem.name_ask != 'Decrypto';
                                                })
                                                console.log(user.name, 'não quer decrypto')
                                            }
                                            // retirando o provider da lista de providers Buenbit 2.0 do usuário 
                                            if (!user.providers_buenbit) {
                                                melhoresRulos_user = melhoresRulos_user.filter((elem) => {
                                                    return elem.name_bid != 'Buenbit 2.0' && elem.name_ask != 'Buenbit 2.0';
                                                })
                                                console.log(user.name, 'não quer buen')
                                            }
                                            // retirando o provider da lista de providers Ripio do usuário 
                                            if (!user.providers_ripio) {
                                                melhoresRulos_user = melhoresRulos_user.filter((elem) => {
                                                    return elem.name_bid != 'Ripio' && elem.name_ask != 'Ripio';
                                                })
                                                console.log(user.name, 'não quer Ripio ')
                                            }
                                            // retirando o provider da lista de providers Ripio Exchange do usuário 
                                            if (!user.providers_ripioExchange) {
                                                melhoresRulos_user = melhoresRulos_user.filter((elem) => {
                                                    return elem.name_bid != 'Ripio Exchange' && elem.name_ask != 'Ripio Exchange';
                                                })
                                                console.log(user.name, 'não quer Ripio Exchange')
                                            }



                                            //  =================  ORDENANDO A LISTA DE MELHORES RULOS  =====================
                                            melhoresRulos_user.sort(function (a, b) {
                                                return a.gain < b.gain ? 1 : a.gain > b.gain ? -1 : 0;
                                            });
                                            melhoresPrices_user.sort(function (a, b) {
                                                return a.gain < b.bid ? 1 : a.gain > b.bid ? -1 : 0;
                                            });
                                            // FIM DO ORDENANDO LISTA


                                            //var result = melhoresRulos_user;


                                            if (melhoresRulos_user != []) {
                                                var dateNow = dateTime.create()['_now']
                                                //console.log(dateNow);
                                                var date1 = user.last_notification_time.toDate()
                                                //console.log(user.last_notification_time.toDate());
                                                var diffMs = (dateNow - date1);

                                                var diffHrs = Math.floor((diffMs % 86400000) / 3600000);
                                                var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
                                                var diff = diffMins + (diffHrs * 60)
                                                //melhoresRulos_user.forEach((e) => {
                                                if (melhoresPrices_user[0].bid >= user.filter_price_notification) {
                                                    console.log('entrou no melhor preço')
                                                    if (diff >= 5) {
                                                        console.log('entrou no +5 preço')
                                                        db.collection('users').doc(doc.id).update({ 'last_notification_time': dateNow, 'last_notification': user.filter_price_notification })
                                                        //notifica a cada 5 min se o preço estiver acima do desejado
                                                        //tem que setar no banco a ultima notificação como o preço setado e data
                                                    } else if (melhoresPrices_user[0].bid > user.last_notification) {
                                                        const token = user.token
                                                        console.log('entrou no else +5 gain')
                                                        sendMsg.config(token, melhoresPrices_user[0], true);
                                                        db.collection('users').doc(doc.id).update({ 'last_notification_time': dateNow, 'last_notification': melhoresPrices_user[0].bid })
                                                        //seta a ultima notificação no banco e a data

                                                    }
                                                }
                                                if (user.filter_gain_notification <= melhoresRulos_user[0].gain) {
                                                    console.log('entrou no melhor ganho')
                                                    if (diff >= 5) {
                                                        console.log('entrou no +5 gain')
                                                        db.collection('users').doc(doc.id).update({ 'last_notification_time_gain': dateNow, 'last_notification_gain': user.filter_gain_notification })
                                                        //notifica a cada 5 min se o preço estiver acima do desejado
                                                        //tem que setar no banco a ultima notificação como o preço setado e data
                                                    } else if (melhoresRulos_user[0].gain > user.last_notification_gain) {
                                                        const token = user.token
                                                        console.log('entrou no else +5 gain')
                                                        sendMsg.config(token, melhoresRulos_user[0], false);
                                                        db.collection('users').doc(doc.id).update({ 'last_notification_time_gain': dateNow, 'last_notification_gain': melhoresRulos_user[0].gain })
                                                        //seta a ultima notificação no banco e a data

                                                    }
                                                }
                                                //})


                                            }

                                            //console.log(result);
                                            //console.log(user.payment_date.toDate())
                                            //console.log(Date.now().toDate())
                                            //console.log(user.payment_date.toDate() > dateTime.create()['_now'])
                                        } else { // parte para os que não querem notificação
                                            console.log(user.name, 'não quer notificação')
                                        }

                                        //console.log(user)
                                    });
                                })
                                .catch((err) => {
                                    console.log('Error getting documents', err);
                                });
                            // Promise.all(console.log(a))

                            // // SALVANDO NO BANDO MELHORES RULOS DE FORMA ORDENADA
                            // var promises = [];
                            // melhoresRulos.forEach((elemento) => {
                            //     //count++;
                            //     // FAZER UPDATE NOS ELEMENTOS QUE JÁ EXISTEM NO BANCO.
                            //     db.collection("best_rulos").where("name_ask", "==", elemento.name_ask).where('name_bid', '==', elemento.name_bid)
                            //         .get()
                            //         .then(function (querySnapshot) {
                            //             querySnapshot.forEach(function (doc) {
                            //                 //console.log(doc.id, " => ", doc.data());
                            //                 // Build doc ref from doc.id

                            //                 promises.push(db.collection("best_rulos").doc(doc.id).update({ 'ask': elemento.ask, 'bid': elemento.bid, 'percent': elemento.percent, 'gain': elemento.gain }));
                            //             });
                            //         })


                            //     //db.collection('best_rulos').add(elemento, { merge: true })

                            // });
                            // Promise.all(promises).then(a =>
                            //     console.log(a))

                        }
                    }) // FIM DO GET DA PLUS CAMBIO





                    console.log('Lista de venda: ', dai_bid_ars)
                }
            })


            console.log('Lista de Compra:', dai_ask_usd)
        }

        //let oportunity_dai = null

        //console.log(res.data)

        // oportunity_dai = res.data.filter((e) => {
        //     return e.coin.indexOf('dai') > -1;
        // })





        // if (parseFloat(oportunity_dai[0].difference) > 0) {

        // }


        //dai_ask = res.data
        // axios.get(hostname + link_dai_bid).then(function (res) {
        //     console.log(res.data)
        // })

    })





    // verificar se está pago


    // verificar se quer notificação


    // db.collection('users').get()
    //     .then((snapshot) => {
    //         snapshot.forEach((doc) => {
    //             // console.log(doc.id, '=>', doc.data());
    //         });
    //     })
    //     .catch((err) => {
    //         console.log('Error getting documents', err);
    //     });
    // db.ref('user').once('value', (snapshot) => {
    //     const data = snapshot.val();
    //     console.log(data)
    // })

    //res.send('Sejam bem vindos ao novo servidor')
}

//module.exports = router;